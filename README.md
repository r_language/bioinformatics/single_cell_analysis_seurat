# Single_Cell_Analysis_Seurat

In this R file you can have a look at a single cell analysis produced with Seurat library in order to do clustering and identify the most differentially expressed genes represented in heatmaps for each groups/clusters. If you want to run the code you will have to change your path to load two files : gbm_data and mmc2-1. We used the information and data from the following article : https://www.cell.com/cell/fulltext/S0092-8674(19)30687-7?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0092867419306877%3Fshowall%3Dtrue#secsectitle0075. In addition, we used the following website to produce the biological networks : https://blca.cregmap.com/



Authors : Marion Estoup, Chloé Saint-Dizier, Narges Jafarzadeh

E-mail : marion_110@hotmail.fr

June 2023



